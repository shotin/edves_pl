-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2022 at 05:13 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pl_edves`
--

-- --------------------------------------------------------

--
-- Table structure for table `prospect_user`
--

CREATE TABLE `prospect_user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `subject_class` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `school_name` text NOT NULL,
  `user_type` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prospect_user`
--

INSERT INTO `prospect_user` (`id`, `email`, `phone`, `class`, `subject_class`, `school`, `role`, `address`, `school_name`, `user_type`, `created_at`) VALUES
(1, 'yusufomotoyosi@gmail.com', '08106030493', 'fsege', 'jjhjjhh', 'sfsfsfss', '', '', '', 'parent', '2022-01-31 15:58:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `prospect_user`
--
ALTER TABLE `prospect_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `prospect_user`
--
ALTER TABLE `prospect_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
