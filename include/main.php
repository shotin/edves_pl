
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">


    <link href="https://fonts.googleapis.com/css2?family=Bubblegum+Sans&family=Delius&display=swap" rel="stylesheet"> 
 
    <link rel="stylesheet" href="./include/css/style.css?vs=3.3">
      <!-- Link Swiper's CSS -->
      <link
      rel="stylesheet"
      href="include/css/swiper.css"
    />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <title>PL | Edves</title>
  </head>
  <body>
<?php
  include 'include/controller/display_main.php';
?>
    <div class="main">
      <!-- NAV BEGINGS -->
      <nav class="navbar navbar-expand-lg navbar-light pb-0">
        <div class="container-fluid px">
          <a class="navbar-brand" href="#">
            <div class="pl-nav">
              PL by Edves
            </div>
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item ps-lg-3">
                <a class="nav-link active" aria-current="page" href="#">Home</a>
              </li>
              <li class="nav-item px-lg-3">
                <a class="nav-link" href="#">For Schools</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">For Parents</a>
              </li>
            </ul>
            <form class="nosubmit d-none d-sm-none d-md-block">
              <input class="nosubmit" type="search" placeholder="What do you want to learn?">
            </form>

            <button class="btn mx-md-3 loginBtn" type="submit">Login</button>
            <button class="btn signupBtn parent" id="" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" type="submit">Sign Up</button>
          </div>
        </div>
      </nav>
      <hr>
      <!-- NAV ENDS -->

      <!-- BANNER SECTION -->
      <div class="container banner">
        <div class="row">
          <div class="col-md-6 order-sm-2 order-md-1 pb-sm-5">
            <div class="heading mb-4">
              <h1>Learn any subject from</h1>
              <h1>top schools in the world</h1>
            </div>
            <div class="sub-heading mb-4">
              <p class="mb-0">Join other top performing students to learn PL by</p>
              <p>Edves and ace all your subjects</p>
            </div>

            <button class="btn btn-lg learnBtn student" id="" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Start Learning Now</button>
          </div>
          <div class="col-md-6 order-1">
            <div class="img-background">
              <img src="./include/img/img.png" class="img-fluid">
            </div>
          </div>
        </div>
      </div>
      <!-- BANNER SECTION ENDS -->

      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 curve">
              <!-- <img src="./include/img/illus.png" style="width: 1200px; padding-left: -150px; margin-top: -30px; "> -->
          </div>
        </div>
      </div>

    </div>

    <section class="bg_logo">
      <div class="container p-5 text-center logo_schools">
             <h3 class="fw-bolder">Connect to tutors from ranking schools from anywhere in the world</h3>

              <!-- Swiper -->            
                  <div class="swiper mySwiper">
                     <div class="swiper-wrapper">
                         <div class="swiper-slide"><img src="include/img/logo.png"  oncontextmenu="return false;" alt="logo"></div>
                         <div class="swiper-slide"><img src="include/img/logo1.png" oncontextmenu="return false;"  alt="logo2"></div>
                         <div class="swiper-slide"><img src="include/img/logo2.png" oncontextmenu="return false;" alt="logo3"></div>
                         <div class="swiper-slide"><img src="include/img/logo3.png" oncontextmenu="return false;"  alt="logo4"></div>
                         <div class="swiper-slide"><img src="include/img/logo4.png" oncontextmenu="return false;"  alt="logo5"></div>
                         <div class="swiper-slide"><img src="include/img/logo5.png" oncontextmenu="return false;" alt="logo6"></div>
                         <div class="swiper-slide"><img src="include/img/logo6.png" oncontextmenu="return false;" alt="logo7"></div>
                         <div class="swiper-slide"><img src="include/img/logo7.png" oncontextmenu="return false;"  alt="logo8"></div>
                         <div class="swiper-slide"><img src="include/img/logo8.png" oncontextmenu="return false;"  alt="logo9"></div>
                         <div class="swiper-slide"><img src="include/img/logo9.png" oncontextmenu="return false;" alt="logo10"></div>
                         <div class="swiper-slide"><img src="include/img/logo10.png" oncontextmenu="return false;" alt="logo11"></div>
                     </div>
                     <div class="swiper-button-next"></div>
                     <div class="swiper-button-prev"></div>
                     <div class="swiper-pagination"></div>
                 </div>
      </div>
   </section>

   <section class="gap_100 all_path">
    <div class="container">
        <div class="row">
             <div class="col-sm-7 right_path">
                <h3 class="text-left">How to help put students on the right path to success <img class="group_path" src="include/img/Group.png" alt=""></h3>
             </div>

             <div class="col-sm-5 access_path">
                 <p>Get access to standard educational content and personalize your learning experience with one-on-one classes</p>
             </div>
        </div>
    </div>
</section>

<section class="mt-5">
  <div class="container all_card">
      <div class="row p-4">
          <div class="col-md-4">
             <div id="background">
                 <div class="row p-3">
                     <img src="include/img/icon.png"  class="img-fluid" alt="" style="width:100px; height:75px">
                 </div>
                 
                  <div class="p-3">
                    <h5 class="fw-bolder">Find a school and subject of interest</h5>
                    <p>Select any topic from our vast database, cutting across various K-12 curricula to suite your specific academic needs</p>
                  </div>
             </div>
          </div>

          <div class="col-md-4">
             <div id="background1">
                 <div class="row p-3">
                     <img src="include/img/icon1.png"  class="img-fluid" alt="" style="width:100px; height:75px">
                 </div>
                 
                  <div class="p-3">
                    <h5 class="fw-bolder">Engage with high quality lessons and assessments</h5>
                    <p>Enjoy engaging educational contents, images, live videos, games, quizzes, audios from tutors around the world</p>
                  </div>
             </div>
          </div>

          <div class="col-md-4">
             <div id="background2">
                 <div class="row p-3">
                     <img src="include/img/icon2.png"  class="img-fluid" alt="" style="width:100px; height:75px">
                 </div>
                 
                  <div class="p-3">
                    <h5 class="fw-bolder">Gain mastery through one-on-one classes</h5>
                    <p>Schedule personalized learning sessions with professional tutors to tackle difficult learning subject areas</p>
                  </div>
             </div>
          </div>
      </div>
  </div>
</section>

<section class="bg_logo all_pl">
  <img class="all_lines" src="include/img/lines.png" alt="">
  <img class="all_circle mt-5" src="include/img/vector.png" alt="">
   <div class="container">
       <div class="row">
         <div class="col-sm-6 text-center">
            <img class="all_student img-fluid" src="include/img/card_img3.png"  alt="">
         </div>

         <div class="col-sm-6 for_students">
             <h5 class="text-center">For Students</h5>
             <h4 class="fw-bolder mt-5">Search and learn any subject from any school directly on your PL by Edves dashboard</h4>
             <p class="mt-3">Students can be enrolled into any subject delivered and assessed by top school brands across the globe to further boost their academic performance and grades</p>
             <button class="btn btn-lg learnBtn student" id="" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Sign Up as a Student</button>
        </div>
    
         <div class="col-sm-12 text-center mt-3">
           <img class="illustrate mt-5 img-fluid" src="include/img/illustration.png" alt="">
        </div>
    </div>
   </div>
</section>

<section class="bg_logo">
  <div class="container">
      <div class="row"> 
           <div class="col-sm-6 for_students for_stu">
               <h5 class="text-center for_parents" style="margin-top: 140px;">For Parents</h5>
               <h4 class="fw-bolder mt-5">Enroll your kids to learn any subject from A-list school across the globe</h4>
               <p class="mt-3">Your child no longer needs to travel long distances just to access quality education. You can now sign them up to learn any subject of interest taught by the best tutors from Ivy League schools on PL by edves.</p>
               <button class="btn btn-lg learnBtn parent" id=""  type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Sign Up Your Child</button>
           </div>

           <div class="col-sm-6 text-center">
             <img class="all_student img-fluid" src="include/img/card_4.png"  alt="">
          </div>
      </div>
  </div>
</section>

<section class="bg_logo">
   <div class="container">
    <div class="row">
         <div class="col-sm-6 text-center">
            <img class="all_student img-fluid" src="include/img/card_5.png"  style="margin-top:160px" alt="">
         </div>

         <div class="col-sm-6 for_students school_sign"  style="margin-top:160px">
             <h5 class="text-center">For Schools</h5>
             <h4 class="fw-bolder  mt-5">An opportunity to teach beyond the four walls of the school</h4>
             <p class="mt-3">Set your school on the map of the world, reposition your brand as an international study option. K-12 learners can now take classes at your school from anywhere in the world on PL by Edves</p>
             <button class="btn btn-lg learnBtn school" id="" type="button" data-bs-toggle="modal" data-bs-target="#schoolModal">Sign Up Here</button>
        </div>
    
        
        <div class="col-sm-12">
           <img class="all_lines1 img-fluid" src="include/img/vector1.png" alt="">
        </div>
    </div>
   </div>
</section>

    <section class="explore p-4">
      <div class="container pt-5 mb-5 d-none d-lg-block">
        <div class="d-flex justify-content-between mb-4">
          <h4>Explore PL by Edves</h4>
          <button class="btn btn-outline-warning radius">See All</button>
        </div>

        <div class="row">
          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 64.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Agricultural Science</h5>
                      <p>12 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 62.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Biology</h5>
                      <p>8 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 65.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Chemistry</h5>
                      <p>20 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row mt-5">
          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 60.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Commerce</h5>
                      <p>32 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 63.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Civil Education</h5>
                      <p>4 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 66.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Current Affairs</h5>
                      <p>33 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row mt-5">
          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 61.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Economics</h5>
                      <p>29 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 67.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>English Language</h5>
                      <p>19 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 64.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Food and Nutrition</h5>
                      <p>2 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

       <!-- MOBILE SECTION-->
      <div class="container pt-5 mb-5 d-sm-block d-lg-none">
        <div class="d-flex justify-content-between mb-4">
          <h4>Exploy PL by Edves</h4>
          <button class="btn btn-outline-warning radius">See All</button>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 64.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Agricultural Science</h5>
                      <p>12 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="my-3 my-sm-0 col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 62.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Biology</h5>
                      <p>8 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row mt-sm-5">
          <div class="col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 60.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Commerce</h5>
                      <p>32 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="my-3 my-sm-0 col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 63.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Civil Education</h5>
                      <p>4 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row mt-sm-5">
          <div class="col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 66.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Current Affairs</h5>
                      <p>33 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="my-3 my-sm-0 col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 61.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Economics</h5>
                      <p>29 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row mt-sm-5">
          <div class="col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 65.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Chemistry</h5>
                      <p>20 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="my-3 my-sm-0 col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 67.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>English Language</h5>
                      <p>19 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row mt-sm-5">

          <div class="col-sm-6">
            <div class="card shadow" id="my_card">
              <div class="card-body" style="margin: -15px;">
                <div class="row">
                  <div class="col-4">
                    <img src="./include/img/Rectangle 64.png" class="img-fluid">
                  </div>
                  <div class="col-8">
                    <div style="padding-top: 20%; padding-left: 5px;">
                      <h5>Food and Nutrition</h5>
                      <p>2 Schools</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class=" mt-4 academic_img">
      <div class="container">
          <div class="row">
            <div class="col-sm-9 p-2">
               <h4 class="fw-bolder w-75">Your academic subject weakness can be become an area of strength</h4>
               <p>Sign up on PL by Edves and become academic stars</p>
            </div>
    
            <div class="col-sm-3 mt-5">
              <button class="btn btn-lg student_btn1 parent" id=""  type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Sign Up For Free!</button>
                <div class="mt-3">
                  <img class="academic_ellli" src="include/img/Ellipse15.png" alt="">
                  <img class="academic_ellli2" src="include/img/Ellipse14.png" alt="">
                  <img class="academic_ellli3" src="include/img/Ellipse13.png" alt="">
                </div>
            </div>
          </div>
      </div>
    </section>

    <section class="footer">
      <div class="container">
        <div class="pl-footer">
          PL by Edves
        </div>
        <div class="row">
          <div class="col-md-3">
            <h5>Categories</h5>
            <p><a href="#">Agricultural Science</a></p>
            <p><a href="#">Biology</a></p>
            <p><a href="#">Chemistry</a></p>
            <p><a href="#">Commerce</a></p>
            <p><a href="#">Economics</a></p>
            <p><a href="#">English Language</a></p>
            <p><a href="#">Food and Nutrition</a></p>
            <p><a href="#">Geography</a></p>
            <p><a href="#">History</a></p>
            <p><a href="#">Integrated Science</a></p>
          </div>
          <div class="col-md-2">
            <h5>Company</h5>
            <p><a href="#">Abous Us</a></p>
            <p><a href="#">Contact Us</a></p>
          </div>
          <div class="col-md-3">
            <h5>Join Us</h5>
            <p><a href="#">Partner with PL by Edves</a></p>
            <p><a href="#">Join as a Student</a></p>
          </div>
          <div class="col-md-2">
            <h5>Help</h5>
            <p><a href="#">Help and Support</a></p>
            <p><a href="#">Careers</a></p>
            <p><a href="#">Blog</a></p>
          </div>
          <div class="col-md-2">
            <h5>Legal</h5>
            <p><a href="#">Terms and Condition</a></p>
            <p><a href="#">Privacy Policy</a></p>
          </div>
        </div>
      </div>
      <div class="copy text-center">
        <p>Copyright &copy 2022. All Rights Reserved</p>
      </div>
    </section>

     <!-- MODAL -->
     <div class="modal custom fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="dialog">
        <div class="modal-content" id="content">
          <div class="modal-header" id="modal-head">
            <button type="button" class="btn-close" id="btnclose" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body pb-5" id="modal-body">
            <h1 class="modal-title text-center" id="exampleModalLabel">Get notified when we launch</h1>
            <div class="row">
              <div class="col-md-8 offset-md-2">
                <p class="text-center">Be the first to know. Join our waiting list by filling your child's details below</p>
              </div>
            </div>
          
            <form action="" method="POST">  

            <?php 
               foreach ($user->getRegisterError() as $error) {
                echo $error;
              }
            ?>
              <div class="mb-3">
                <label for="email" class="col-form-label">Email</label>
                <input type="email" required class="form-control field" placeholder="edves@gmail.com" name="email">
              </div>

              <div class="mb-3">
                <label for="phone" class="col-form-label">Phone Number</label>
                <!-- <input type="tel" required placeholder="+234 703 203 2343" class="form-control field" name="phone"> -->
                <div class="input-group mb-3">
                  <button class="btn btn-light" type="button" data-bs-toggle="dropdown" ><img src="include/img/ghana.jpg" alt="" style="width: 20px;">
                    &nbsp;<i class="fas fa-angle-down mt-2"></i></button>
                  <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">+233</a></li>
                    <li><a class="dropdown-item" href="#">+91</a></li>
                  </ul>
                  <input type="tel" name="phone" required class="form-control" placeholder="+234 703 203 2343">
                </div>
              </div>

              <div class="mb-3">
                <label for="class" class="col-form-label">Class of Children</label>
                <input type="text" required class="form-control field" name="class" placeholder="Grade8, Grade10">
                <span class="text-danger">Separate each class with a comma</span>
              </div>

              <div class="mb-3">
                <label for="subject" class="col-form-label">Subject of Interest</label>
                <input type="text" required class="form-control field" name="subject_class" placeholder="English, Mathematics, Biology">
                <span class="text-danger">Separate each subject with a comma</span>
              </div>

              <div class="mb-3">
                <label for="recipient-name" class="col-form-label">School of Interest</label>
                <input type="text" required class="form-control field" name="school" placeholder="Temple Schools, Atlantic Hall">
                <span class="text-danger">Separate each school with a comma</span>
              </div>

              <div class="mb-3">
                <input type="hidden" required class="form-control field" id="user_type" name="user_type" placeholder="Temple Schools, Atlantic Hall">
              </div>
              <div class="d-grid gap-2">
                <button name="submit" class="btn learnBtn">Notify Me</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- MODAL FOR SCHOOL -->
    <div class="modal custom fade" id="schoolModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="dialog">
        <div class="modal-content" id="content">
          <div class="modal-header" id="modal-head">
            <button type="button" class="btn-close" id="btnclose" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body pb-5" id="modal-body">
            <h1 class="modal-title text-center" id="exampleModalLabel">Great to have you onboard</h1>
            <div class="row">
              <div class="col-md-8 offset-md-2">
                <p class="text-center">Join over 20 schools across the world to be a part of the academic success stories of students</p>
              </div>
            </div>
            <form action="" method="POST">
            <?php 
               foreach ($user->getRegisterSchoolError() as $error) {
                echo $error;
              }
            ?>
              <div class="mb-3">
                <label for="email" class="col-form-label">School Name</label>
                <input type="text" name="school_name" required class="form-control field" placeholder="Type in your school name">
              </div>

              <div class="mb-3">
                <label for="phone" class="col-form-label">Phone Number</label>
                <!-- <input type="text" required placeholder="+234 703 203 2343" class="form-control field" name="phone"> -->
                <div class="input-group mb-3">
                  <button class="btn btn-light" type="button" data-bs-toggle="dropdown" ><img src="include/img/ghana.jpg" alt="" style="width: 20px;">
                    &nbsp;<i class="fas fa-angle-down mt-2"></i></button>
                  <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">+233</a></li>
                    <li><a class="dropdown-item" href="#">+91</a></li>
                  </ul>
                  <input type="tel" name="phone" required class="form-control" placeholder="+234 703 203 2343">
                </div>
              </div>

              <div class="mb-3">
                <label for="class" class="col-form-label">Email</label>
                <input type="email" required class="form-control field" name="email" placeholder="Type in your email address">
              </div>

              <div class="mb-3">
                <label for="subject" class="col-form-label">Role in school</label>
                <input type="text" required class="form-control field" name="role" placeholder="What is your role in school?">
              </div>

              <div class="mb-3">
                <label for="recipient-name" class="col-form-label">School Address</label>
                <input type="text" required class="form-control field" name="address" placeholder="Where is your school located?">
              </div>

              <div class="mb-3">
                <input type="hidden" required class="form-control field" id="use" name="user_type" placeholder="Temple Schools, Atlantic Hall">
              </div>

              <div class="d-grid gap-2">
                <button  name="submit_school" class="btn learnBtn">Onboard Me</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="text-center mt-3 container">
        <button type="button" class="btn-close float-end p-2" data-bs-dismiss="modal" aria-label="Close"></button>
        <h5 style="font-size: 44px;" class="modal-title p-3 mt-5" id="exampleModalLabel">Get notified when we launch</h5>
        <p class="mx-auto w-50">Be the first to know. Join our waiting list by filling your child's datails below </p>
       
      </div>
      <div class="modal-body mx-auto" style="width: 60%;">
          <form action="" method="POST">
            
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Email</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" required placeholder="Type in your email">      
            </div>

            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Phone Number</label>
              <div class="input-group mb-3">
                <button class="btn btn-light" type="button" data-bs-toggle="dropdown" ><img src="include/img/ghana.jpg" alt="" style="width: 20px;">
                  &nbsp;<i class="fas fa-angle-down mt-2"></i></button>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">+233</a></li>
                  <li><a class="dropdown-item" href="#">+91</a></li>
                </ul>
                <input type="tel" name="phone" required class="form-control" placeholder="+234 Type in your number">
              </div>
            </div>

            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Class of Children</label>
              <input type="text" name="class" class="form-control" id="exampleInputEmail1" placeholder="Type in the class of your child/children" required>
               <div class="form-text text-info">Separate each class with a comma</div>
            </div>

            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Subject of Interest</label>
              <input type="text" name="subject_class" class="form-control" id="exampleInputEmail1" placeholder="Type in your child/children's subject of interest" required>
              <div class="form-text text-info">Separate each subject with a comma</div>
            </div>

            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">School of Interest</label>
              <input type="text" name="school" class="form-control" id="exampleInputEmail1" placeholder="Type in your child/children's school of interest" required>
              <div class="form-text text-info">Separate each subject with a comma</div>
            </div>
            <button name="submit" type="submit" class="btn btn-lg w-100 text-dark learnBtn1 mb-5">Notify me</button>
          </form>
      </div>
    </div>
  </div>
</div> -->

      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

       <!-- Swiper JS -->
       <script src="include/js/swiper.js"></script>

       <!-- Optional JavaScript; choose one of the two! -->
   
       <!-- Option 1: Bootstrap Bundle with Popper -->
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   
       <!-- Option 2: Separate Popper and Bootstrap JS -->
       <!--
       <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
       -->
     
   
         <!-- Initialize Swiper -->
         <script>
          if (window.history.replaceState ) {
                  window.history.replaceState (null, null, window.location.href);
          }

           var swiper = new Swiper(".mySwiper", {
             speed: 1900,
             slidesPerView: 8,
             slidesPerGroup: 8,
             loop: true,
             loopFillGroupWithBlank: true,
             pagination: {
               el: ".swiper-pagination",
               clickable: true,
             },
             navigation: {
               nextEl: ".swiper-button-next",
               prevEl: ".swiper-button-prev",
             },
           });

           $('.student').on('click', function(){
              $('#user_type').val('student');
          });

          $('.parent').on('click', function(){
              $('#user_type').val('parent');
          });

          $('.school').on('click', function(){
              $('#use').val('school');
          });
         </script>
     </body>
   </html>