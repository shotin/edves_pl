<?php
 class User extends Database {

    private $conn;
    private $registerErrorArray;
    private $registerSchoolErrorArray;

    public function __construct() {
      $this->conn = $this->connect();
      $this->registerErrorArray = array();
      $this->registerSchoolErrorArray = array();
   }

   public function register($email, $phone, $class, $subject_class, $school, $user_type) {
      $this->ValidateEmail($email);
   
      if (empty($this->registerErrorArray) == true) {
         //insert user
         return $this->insertUserDetails($email, $phone, $class, $subject_class, $school, $user_type);
      }else
      return null;
   }

   public function insertUserDetails($email, $phone, $class, $subject_class, $school, $user_type) 
   {
         $sql = "INSERT INTO prospect_user(email, phone, class, subject_class, school, user_type) VALUES(?,?,?,?,?,?)";
         $stmt = $this->conn->prepare($sql);

         try {
            $stmt->execute([$email, $phone, $class, $subject_class, $school, $user_type]);
            return true;

         } catch (Exception $e) {
            echo $e->getMessage();
            return false;
         }
    }
    

    public function ValidateEmail($email) {
      $sql = "SELECT email FROM prospect_user WHERE email =  ? LIMIT 1";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute([$email]);
      $result = $stmt->fetch();

      if ($result != null) {
       array_push($this->registerErrorArray, "<script>
            Swal.fire({
                title: 'Error!',
                text: 'This email is already in use',
                icon: 'error',
                confirmButtonText: 'Okay'
            });      
            </script>");
         return $result;
      }
   }

      public function getRegisterError()
   {
      return $this->registerErrorArray; 
   }


    
//Submitting for schools
   public function registerSchool($school_name, $phone, $email, $role, $address, $user_type) {
      $this->ValidateEmailSchool($email);
   
      if (empty($this->registerSchoolErrorArray) == true) {
         //insert user
         return $this->insertSchoolUserDetails($school_name, $phone, $email, $role, $address, $user_type);
      }else
      return null;
   }

   
   public function insertSchoolUserDetails($school_name, $phone, $email, $role, $address, $user_type){
      //   $sql = "INSERT INTO user(email, phone, class, subject_class, school) VALUES(?, ?, ?, ?, ?)";
      //   $stmt= $this->conn->prepare($sql);
         $sql = "INSERT INTO prospect_user(school_name, phone, email, role, address, user_type) VALUES(?,?,?,?,?,?)";
         $stmt = $this->conn->prepare($sql);

         try {
            $stmt->execute([$school_name, $phone, $email, $role, $address, $user_type]);
            return true;

         } catch (Exception $e) {
            echo $e->getMessage();
            return false;
         }
    }

   public function ValidateEmailSchool($email) {

      $sql = "SELECT email FROM prospect_user WHERE email = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute([$email]);
      $result = $stmt->fetch();

      if ($result != null) {
         array_push($this->registerSchoolErrorArray, "
         <script>
            Swal.fire({
                title: 'Error!',
                text: '  This email is already in use',
                icon: 'error',
                confirmButtonText: 'Okay'
            });      
            </script>");
         return;
      }
   }




   public function getRegisterSchoolError()
   {
      return $this->registerSchoolErrorArray; 
   }
 }