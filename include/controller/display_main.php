<?php
include 'include/config/db.php';
include 'include/controller/User.php';

$user = new User();
$ERROR = '';

if (isset($_POST['submit'])) {
   $email  =             $_POST['email'];
   $phone  =             $_POST['phone'];
   $class  =             $_POST['class'];
   $subject_class  =     $_POST['subject_class'];
   $school  =   $_POST['school'];
   $user_type  =  $_POST['user_type'];
   $result = $user->register( $email, $phone, $class, $subject_class, $school, $user_type);

    if ($result != null) {
       echo  "<script>
                Swal.fire({
                    title: 'Yayy!!',
                    text:  'We have received your request, kindly expect our email as soon as we launch',
                    timer: 5000,
                    imageUrl: 'include/img/group3.png',
                    imageWidth: 200,
                    imageHeight: 100,
                    showCancelButton:false,
                    showConfirmButton:false
                  })
            </script>";
    }
}

if (isset($_POST['submit_school'])) {
  $school_name =   $_POST['school_name'];
  $phone        =   $_POST['phone'];
  $email        =   $_POST['email'];
  $role         =   $_POST['role'];
  $address      =   $_POST['address'];
  $user_type    =     $_POST['user_type'];

  $result = $user->registerSchool($school_name, $phone, $email, $role, $address,  $user_type);

   if ($result != null) {
      echo  "<script>
               Swal.fire({
                  title: 'Yayy!!',
                  text:  'We have received your request, kindly expect our email as soon as we launch',
                  timer: 5000,
                  imageUrl: 'include/img/group3.png',
                  imageWidth: 200,
                  imageHeight: 100,
                  showCancelButton:false,
                  showConfirmButton:false
                 })
           </script>";
   }
}